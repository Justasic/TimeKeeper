/* BSD 3-Clause License
 * 
 * Copyright (c) 2022, Justin Crawford
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <vector>
#include <cassert>
#include "models/WorkDay.h"
// using sqlite_result = std::vector<std::map<std::string, sql::sqlite_data_type>>;

WorkDay::WorkDay(const std::map<std::string, sql::sqlite_data_type> &chunk, sql::sqlite sdb) : id(0), timein(0), timeout(0)
{
	try 
	{
		this->id      = std::get<decltype(WorkDay::id)>(chunk.at("id"));
		this->timein  = std::get<decltype(WorkDay::timein)>(chunk.at("timein"));
		if (auto &&value = chunk.at("timeout"); value.index() == 0)
			this->timeout = std::get<decltype(WorkDay::timeout)>(value);

		// Foreign Keys are a bit more complex, we must make another request to the database.
		sql::sqlite_result sqlres;
		// TODO: error checking
		sdb.select_star("PayPeriod", "WHERE id = :id", std::vector<sql::where_binding>{{"id", std::get<int64_t>(chunk.at("period"))}}, sqlres);
		assert(((void)"Foreign Key returned more than 1 value!!", sqlres.size() == 1));
		this->period = PayPeriod(sqlres[0], sdb);
	}
	catch (const std::exception &ex)
	{
		fmt::print(stderr, "Failed to deserialize `WorkDay`: {}\n", ex.what());
		throw;
	}
}

nlohmann::json WorkDay::serialize() const
{
	return nlohmann::json {
		{"id", this->id},
		{"timein", this->timein},
		{"timeout", this->timeout},
		{"period", this->period.serialize()}
	};
}

int WorkDay::InitializeTable(sql::sqlite sdb)
{
	constexpr char WorkDayTable[] = R"(
		CREATE TABLE IF NOT EXISTS WorkDay (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			period INTEGER,
			timein INTEGER NOT NULL,
			timeout INTEGER,
			FOREIGN KEY (period) REFERENCES PayPeriod(id) ON DELETE CASCADE
		)
	)";

	char *errmsg = nullptr;
	if (int rc = sqlite3_exec(sdb.sqlitedb(), WorkDayTable, nullptr, nullptr, &errmsg); rc != SQLITE_OK || errmsg != nullptr)
	{
		fprintf(stderr, "Failed to create the `WorkDay` table: %s\n", errmsg);
		sqlite3_free(errmsg);
		return EXIT_FAILURE;
	}

	return 0;
}
/* BSD 3-Clause License
 * 
 * Copyright (c) 2022, Justin Crawford
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <vector>
#include "models/Company.h"
// using sqlite_result = std::vector<std::map<std::string, sql::sqlite_data_type>>;

Company::Company(const std::map<std::string, sql::sqlite_data_type> &chunk) : id(0), rate(0.0f), active(false)
{
	try 
	{
		this->id          = std::get<decltype(Company::id)>(chunk.at("id"));
		this->name        = std::get<decltype(Company::name)>(chunk.at("name"));
		this->description = std::get<decltype(Company::description)>(chunk.at("description"));
		this->rate        = std::get<decltype(Company::rate)>(chunk.at("rate"));
		this->active      = static_cast<bool>(std::get<int64_t>(chunk.at("active")));

	}
	catch (const std::exception &ex)
	{
		fmt::print(stderr, "Failed to deserialize `Company`: {}", ex.what());
		throw;
	}
}

nlohmann::json Company::serialize() const
{
	return nlohmann::json {
		{"id", this->id},
		{"name", this->name},
		{"description", this->description},
		{"rate", this->rate},
		{"active", this->active},
	};
}

int Company::InitializeTable(sql::sqlite sdb)
{
	constexpr char CompanyTable[] = R"(
		CREATE TABLE IF NOT EXISTS Company (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			name VARCHAR(255),
			description VARCHAR(255),
			rate DOUBLE,
			active BOOLEAN DEFAULT TRUE
		)
	)";

	char *errmsg = nullptr;
	if (int rc = sqlite3_exec(sdb.sqlitedb(), CompanyTable, nullptr, nullptr, &errmsg); rc != SQLITE_OK || errmsg != nullptr)
	{
		fprintf(stderr, "Failed to create the `Company` table: %s\n", errmsg);
		sqlite3_free(errmsg);
		return -1;
	}

	return 0;
}
/* BSD 3-Clause License
 * 
 * Copyright (c) 2022, Justin Crawford
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <vector>
#include <cassert>
#include "models/PayPeriod.h"
// using sqlite_result = std::vector<std::map<std::string, sql::sqlite_data_type>>;

PayPeriod::PayPeriod(const std::map<std::string, sql::sqlite_data_type> &chunk, sql::sqlite sdb) : id(0), rate(0.0f), begin(0), end(0)
{
	try 
	{
		this->id    = std::get<decltype(PayPeriod::id)>(chunk.at("id"));
		this->begin = std::get<decltype(PayPeriod::begin)>(chunk.at("begin"));
		if (auto &&value = chunk.at("end"); value.index() == 0)
			this->end   = std::get<decltype(PayPeriod::end)>(value);
		this->rate  = std::get<decltype(PayPeriod::rate)>(chunk.at("rate"));

		// Foreign Keys are a bit more complex, we must make another request to the database.
		sql::sqlite_result sqlres;
		// TODO: error checking
		sdb.select_star("Company", "WHERE id = :id", std::vector<sql::where_binding>{{"id", std::get<int64_t>(chunk.at("company"))}}, sqlres);
		assert(((void)"Foreign Key returned more than 1 value!!", sqlres.size() == 1));
		this->company = Company(sqlres[0]);
	}
	catch (const std::exception &ex)
	{
		fmt::print(stderr, "Failed to deserialize `PayPeriod`: {}\n", ex.what());
		throw;
	}
}

nlohmann::json PayPeriod::serialize() const
{
	return nlohmann::json {
		{"id", this->id},
		{"company", this->company.serialize()},
		{"rate", this->rate},
		{"begin", this->begin},
		{"end", this->end},
	};
}

int PayPeriod::InitializeTable(sql::sqlite sdb)
{
	constexpr char PeriodTable[] = R"(
		CREATE TABLE IF NOT EXISTS PayPeriod (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			company INTEGER,
			rate DOUBLE,
			begin INTEGER NOT NULL,
			end INTEGER,
			FOREIGN KEY (company) REFERENCES Company(id) ON DELETE CASCADE
		)
	)";

	char *errmsg = nullptr;
	if (int rc = sqlite3_exec(sdb.sqlitedb(), PeriodTable, nullptr, nullptr, &errmsg); rc != SQLITE_OK || errmsg != nullptr)
	{
		fprintf(stderr, "Failed to create the `PayPeriod` table: %s\n", errmsg);
		sqlite3_free(errmsg);
		return EXIT_FAILURE;
	}

	return 0;
}
/* BSD 3-Clause License
 * 
 * Copyright (c) 2022, Justin Crawford
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <exception>
#include <fmt/core.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <unordered_map>
#include "cmdline.h"
#include "exceptions.h"

#include "models/Company.h"
#include "models/PayPeriod.h"
#include "models/WorkDay.h"
// Included by shell command execution of compiler
// #include "vendor.h"

using namespace nlohmann;

INCTXT(LicenseFile, "../LICENSE");

extern std::unordered_map<std::string, std::unordered_map<std::string, std::string>> StaticAssets;

int main(int argc, char **argv)
{
	auto [ret, cmdlinecfg] = CLIFormatter::ParseCommandLineArguments(argc, argv);
	if (ret != 0)
		return EXIT_FAILURE;

	httplib::Server svr;
	sql::sqlite sdb;
	char** pStatement=NULL;

	sqlite3_config(SQLITE_CONFIG_LOG, [](void * pStatement, int iErrCode, const char *zMsg)
	{
		fmt::print(stderr, "{}\n({}) {}\n", *(char**)pStatement, iErrCode, zMsg);
	}, pStatement);
	
	if (sdb.open(cmdlinecfg->DatabaseFilePath) != SQLITE_OK)
	{
		fmt::print(stderr, "Failed to open {}\n", cmdlinecfg->DatabaseFilePath);
		return EXIT_FAILURE;
	}

	// Set SQLITE trace logging
	if (cmdlinecfg->SQLDebugFlag)
	{
		sqlite3_trace_v2(sdb.sqlitedb(), SQLITE_TRACE_STMT, [](unsigned InvokeReason, void *pCtx, void *pStatement, void *sText){
			char *expanded = sqlite3_expanded_sql(reinterpret_cast<sqlite3_stmt*>(pStatement));
			// fmt::print("SQL Statement: {}\n", reinterpret_cast<const char*>(sText));
			fmt::print("SQL Statement: {}\n", expanded);
			sqlite3_free(expanded);
			return SQLITE_OK;
		}, nullptr);
	}

	/********************************************************/
	// Setup error handling and all that fun jazz
	//
	svr.set_logger([](const httplib::Request &req, const httplib::Response &res)
	{
		std::string addr = req.remote_addr;
		if (addr.find(":") != std::string::npos)
			addr = fmt::format("[{}]", addr);
		fmt::print("{} request from {}:{} for {} status {}\n", req.method, addr, req.remote_port, req.path, res.status);
	});

	svr.set_error_handler([](const httplib::Request& req, httplib::Response& res) {
		const char *&&status = httplib::detail::status_message(res.status);
		res.set_content(fmt::format("<p><span style='color:red;'>{}</span> {}</p>", res.status, status), "text/html");
	});

	svr.set_exception_handler([](const httplib::Request& req, httplib::Response& res, std::exception_ptr ep) {
		try 
		{
			if (ep)
				std::rethrow_exception(ep);
		}
		catch (const NotFoundException &e)
		{
			res.status = 404;
			return;
		}
		catch (const ValidationException &e)
		{
			fmt::print(stderr, "Caught exception: {}\n", e.what());
			res.status = 400;
			return;
		}
		catch (const std::exception &e)
		{
			fmt::print(stderr, "Caught exception: {}\n", e.what());
		}
		catch (...)
		{
			fmt::print(stderr, "An unknown exception was thrown!\n");
		}
		res.status = 500;
	});
	/********************************************************/

	// Enable foreign_keys
	char *errmsg = nullptr;
	if (int rc = sqlite3_exec(sdb.sqlitedb(), "PRAGMA foreign_keys = ON;", nullptr, nullptr, &errmsg); rc != SQLITE_OK || errmsg != nullptr)
	{
		fprintf(stderr, "Failed to enable foreign keys: %s\n", errmsg);
		sqlite3_free(errmsg);
		return EXIT_FAILURE;
	}
	
	// Create the tables if they don't exist
	if (Company::InitializeTable(sdb) != 0)
		return EXIT_FAILURE;

	if (PayPeriod::InitializeTable(sdb) != 0)
		return EXIT_FAILURE;

	if (WorkDay::InitializeTable(sdb) != 0)
		return EXIT_FAILURE;

	svr.Get("/", [sdb](const httplib::Request& req, httplib::Response& res) 
	{
		json data;
		sql::sqlite_result sqlres;

		if (int rc = sdb.select_star("Company", sqlres); rc != SQLITE_OK)
			throw SQLException("Failed to perform SELECT query on `Company` table: {} ({})", sdb.get_last_error_description(), rc);

		if (!sqlres.empty())
		{
			data = {{"companies", {}}};

			for (auto && it : sqlres)
			{
				Company comp(it);

				if (comp.active)
					data["companies"] += comp.serialize();
				else 
				{
					if (!data.contains("inactivecompanies"))
						data += {"inactivecompanies", {}};
					data["inactivecompanies"] += comp.serialize();
				}
			}
		}

		inja::Environment env;
		env.set_search_included_templates_in_files(false);
		env.include_template("skeleton", env.parse(StaticAssets["templates"]["skeleton"]));
		res.set_content(env.render(StaticAssets["templates"]["landing"], data), "text/html");
	});

	// Render an individual company page, showing the clock-in/clock-out button as well as the
	// hours worked, wages earned, and settings for said company
	svr.Get(R"(/company/(\d+))", [sdb](const httplib::Request& req, httplib::Response& res) 
	{
		json data{
			{"clockedin", false}
		};
		sql::sqlite_result sqlres;
		std::string &&company_id = req.matches[1];

		if (int rc = sdb.select_star("Company", "WHERE id = :id", std::vector<sql::where_binding>{{"id", company_id}}, sqlres); rc != SQLITE_OK)
			throw SQLException("Failed to select `Company` record for id {} table: {}", company_id, sdb.get_last_error_description());
		
		if (sqlres.empty())
			throw NotFoundException("Company {} not found.", company_id);

		data["company"] = Company(sqlres[0]).serialize();
		sqlres.clear();

		if (int rc = sdb.select_star("PayPeriod", "WHERE company = :id", std::vector<sql::where_binding>{{"id", company_id}}, sqlres); rc != SQLITE_OK)
			throw SQLException("Failed to select `PayPeriod` record(s) for id {} table: {}", company_id, sdb.get_last_error_description());

		for (auto &&it : sqlres)
		{
			PayPeriod period(it, sdb);
			if (!period.end)
			{
				data["currentperiod"] = period.serialize();

				// Also query for all the current workdays
				sql::sqlite_result workdayres;
				if (int rc = sdb.select_star("WorkDay", "WHERE period = :id", std::vector<sql::where_binding>{{"id", period.id}}, workdayres); rc != SQLITE_OK)
					throw SQLException("Failed to select `WorkDay` record(s) for id {} table: {}", period.id, sdb.get_last_error_description());

				if (!workdayres.empty())
				{
					data["currentworkdays"] = {};
					for (auto &&wd : workdayres)
					{
						WorkDay wkd(wd, sdb);
						data["currentworkdays"].emplace_back(wkd.serialize());
						if (!wkd.timeout)
							data["clockedin"] = true;
					}
				}
			}

			data["payperiods"].emplace_back(period.serialize());
		}

		fmt::print("Data: {}\n", data.dump());

		inja::Environment env;
		env.set_search_included_templates_in_files(false);
		env.include_template("skeleton", env.parse(StaticAssets["templates"]["skeleton"]));
		res.set_content(env.render(StaticAssets["templates"]["company"], data), "text/html");
	});

	svr.Post(R"(/company/(\d+)/clockstate)", [sdb](const httplib::Request& req, httplib::Response& res) {
		if (!req.has_param("clockstate"))
			throw ValidationException("`clockstate` variable does not exist");

		auto &&clockstate = req.get_param_value("clockstate");

		if (clockstate.empty() || clockstate.find_first_not_of("01") != std::string::npos)
			throw ValidationException("`clockstate` variable is empty or not 1 or 0");

		// First, select the column.
		sql::sqlite_result workdayres;
		if (int rc = sdb.select_star("WorkDay", "WHERE timeout IS NULL", {}, workdayres); rc != SQLITE_OK)
			throw SQLException("Failed to select `WorkDay` record(s) that are null: {}\n", sdb.get_last_error_description());

		std::vector<sql::column_values> params {
			{""}
		};


	});

	svr.Get(R"(/addcompany/)", [](const httplib::Request& req, httplib::Response& res) 
	{
		json data;

		inja::Environment env;
		env.set_search_included_templates_in_files(false);
		env.include_template("skeleton", env.parse(StaticAssets["templates"]["skeleton"]));
		res.set_content(env.render(StaticAssets["templates"]["addcompany"], data), "text/html");
	});

	svr.Post(R"(/addcompany/)", [sdb](const httplib::Request& req, httplib::Response& res) 
	{
		if (!req.has_param("name") || !req.has_param("description") || !req.has_param("payrate"))
			throw ValidationException("`name`, `description`, or `payrate` param does not exist.");

		auto &&name = req.get_param_value("name");
		auto &&description = req.get_param_value("description");
		auto &&payrate = req.get_param_value("payrate");

		if (name.empty() || description.empty() || payrate.empty() || payrate.find_first_not_of("1234567890.-") != std::string::npos)
			throw ValidationException("`name`, `description`, or `payrate` param is empty or `payrate` param contains a non-number character");

		std::vector<sql::column_values> params {
			{"name", name},
			{"description", description},
			{"rate", stof(payrate)}
		};

		if (sdb.insert_into("Company", params) != SQLITE_OK)
			throw SQLException("Failed to insert record into `Company` table: {}", sdb.get_last_error_description());

		// Redirect back to the list, which will now be refreshed.
		res.status = 302;
		res.set_header("Location", "/");
	});

	// Handle rendering static assets (images, javascript, stylesheets, whatever)
	svr.Get(R"(/static/(css|js|img)/([^\/\#]+))", [&](const httplib::Request& req, httplib::Response& res)
	{
		// Serve statics
		std::string &&type = req.matches[1];
		std::string &&asset = req.matches[2];

		// Future mimetypes we might expand upon if necessary
		static std::map<std::string, std::string> mime_map;

		if (auto it = StaticAssets[type].find(asset); it != StaticAssets[type].end())
		{
			// Unknown types should be application/octet-stream
			std::string mime = "application/octet-stream";

			if (type == "css")
				mime = "text/css";
			else if (type == "js")
				mime = "text/javascript";
			else if (type == "img")
			{
				const char *str = httplib::detail::find_content_type(asset, mime_map);
				if (str)
					mime = str;
			}

			res.set_content(it->second, mime);
		}
		else
			res.status = 404;
	});

	// Satisfy license requirement
	svr.Get("/license",[](const httplib::Request& req, httplib::Response& res) { res.set_content(gLicenseFileData, "text/plain"); });

	fmt::print("listening on http://{}:{}/\n", cmdlinecfg->BindAddress, cmdlinecfg->BindPort);

	svr.listen(cmdlinecfg->BindAddress, cmdlinecfg->BindPort);

	return EXIT_SUCCESS;
}
/* BSD 3-Clause License
 * 
 * Copyright (c) 2022, Justin Crawford
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <iostream>
#include <cstdlib>
#include <string_view>
#include <random>
#include <charconv>
#include "cmdline.h"
#ifdef _WIN32
# define WIN32_LEAN_AND_MEAN 1
# include <Windows.h>
#endif

CLIFormatter::CLIFormatter() : VersionFlag(false), LicenseFlag(false), SQLDebugFlag(false), ConfigFilePath("/etc/TimeKeeper.conf") {}

std::string CLIFormatter::make_description(const CLI::App *app) const
{
	return app->get_description();
}

std::string CLIFormatter::GetProgramName(const std::string_view progname)
{
	// convert the argument to a path
	std::filesystem::path path = progname;
	std::string           name = path.filename();
	if (name.empty())
	{
#ifndef _WIN32
		// Dereference /proc/self/exe's symlink on *nix systems
		path = std::filesystem::absolute("/proc/self/exe");
		name = path.filename();
#else
		char buffer[MAX_PATH];
		if (!GetModuleFileNameA(nullptr, buffer, sizeof(buffer)))
		{
			char errbuf[513];
			DWORD err = GetLastError();
			if (!err)
				printf("unknown error occured\n");
			else
			{
				FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, err, 0, errbuf, 512, NULL);
				fmt::print("Error getting exe name: {} ({})\n", errbuf, err);
			}
		}
#endif
	}

	if (name.empty())
		name = "program";

	return name;
}

std::pair<int, std::shared_ptr<CLIFormatter>> CLIFormatter::ParseCommandLineArguments(int argc, char** argv)
{

	CLI::App cliargs{"Bundled web application for TimeKeeper, the employment hours tracker\n", CLIFormatter::GetProgramName(argc ? argv[0] : "program")};

	// Specify formatting for help and whatnot
	std::shared_ptr<CLIFormatter> fmt = std::make_shared<CLIFormatter>();
	fmt->Arguments = std::vector<std::string>(argv, argv+argc);
	fmt->label("REQUIRED", "\033[91mREQUIRED\033[0m");
	fmt->label("Usage", "\033[94mUsage\033[0m");
	fmt->label("OPTIONS", "\033[95mOPTIONS\033[0m");
	fmt->label("SUBCOMMAND", "\033[92mSUBCOMMAND\033[0m");
	fmt->label("SUBCOMMANDS", "\033[92mSUBCOMMANDS\033[0m");
	cliargs.formatter(fmt);

	cliargs.add_flag("--version", fmt->VersionFlag, "Print version and license information");
	cliargs.add_flag("--licenses", fmt->LicenseFlag, "Print OSS license information");
	cliargs.add_flag("--sqldebug", fmt->SQLDebugFlag, "Print SQL statements");
	// cliargs.add_option("-v,--verbose", ThisInstance->Verbosity, "Verbosity level (1 to 6)");
	// cliargs.add_option("-c,--config", fmt->ConfigFilePath, "Configuration file location")->check(CLI::ExistingFile);
	cliargs.add_option("-d,--database", fmt->DatabaseFilePath, "Database file")->default_val("TimeKeepHours.db");
	cliargs.add_option("-b,--bind", fmt->BindAddress, "Interface to bind to")->default_val("0.0.0.0");
	cliargs.add_option("-p,--port", fmt->BindPort, "Port to bind to")->default_val(1234)->check(CLI::Range(1, 65536));

	try
	{
		cliargs.parse(argc, argv);
	}
	catch (const CLI::ParseError &e)
	{
		int ret = cliargs.exit(e);
		if (ret)
			fmt::print(stderr, "error({}): {}\n", ret, e.what());
		return std::make_pair(ret ? -ret : -1, fmt);
	}

	if (fmt->VersionFlag)
	{
		puts(fmt->make_description(&cliargs).c_str());
		return std::make_pair(-1, fmt);
	}
	
	return std::make_pair(0, fmt);
}
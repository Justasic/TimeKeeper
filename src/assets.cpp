/* BSD 3-Clause License
 * 
 * Copyright (c) 2022, Justin Crawford
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <unordered_map>
#include <string>
#include "incbin.h"

/* Templates */
INCTXT(SkeletonTemplate, "../templates/skeleton.html");
INCTXT(CompanyTemplate, "../templates/company.html");
INCTXT(LandingTemplate, "../templates/landing.html");
INCTXT(AddCompanyTemplate, "../templates/addcompany.html");

/* CSS files */
INCTXT(NormalizeCSS, "../static/css/normalize.css");
INCTXT(SkeletonCSS, "../static/css/skeleton.css");
INCTXT(GlobalCSS, "../static/css/global.css");
INCTXT(LandingCSS, "../static/css/landing.css");
INCTXT(CompanyCSS, "../static/css/company.css");
INCTXT(AddCompanyCSS, "../static/css/addcompany.css");

std::unordered_map<std::string, std::unordered_map<std::string, std::string>> StaticAssets
{
	{"templates", {
		{"skeleton", gSkeletonTemplateData},
		{"company", gCompanyTemplateData},
		{"addcompany", gAddCompanyTemplateData},
		{"landing", gLandingTemplateData}
	}},
	{"css", {
		{"normalize.css", gNormalizeCSSData},
		{"skeleton.css", gSkeletonCSSData},
		{"global.css", gGlobalCSSData},
		{"landing.css", gLandingCSSData},
		{"company.css", gCompanyCSSData},
		{"addcompany.css", gAddCompanyCSSData},
	}},
	{"js", {

	}},
	{"img", {

	}}
};




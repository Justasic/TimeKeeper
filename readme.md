# TimeKeeper

This is just a dumb little project I created so I can track my hours when working for my employers. Many times I am in charge of my own hours and adding them up to give the total hours worked and how much money must be exchanged. This site is intended to have like 1 user so don't bother trying to scale it (spoiler: it won't) or use it for some company somewhere.

I intend to only have it be an "in" and "out" button with a table showing all the different hours until the pay period ends where it calculates the actual totals.

## Compiling

This project is intended to be statically compiled, and much of the makefile is around my own custom toolchain I developed using clang, musl-libc, and libc++. You must first modify the makefile to change the compiler and also possibly remove the `-static` flag given. No guarantees this even works outside my environment so good luck.

1. `cd <folder with this readme>`
2. `make`
3. `build/TimeKeeper --help`

## License

BSD 3-Clause License

Copyright (c) 2022, Justin Crawford
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

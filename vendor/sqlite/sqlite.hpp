// sqlite - a thin c++ wrapper of sqlite c library
// Taken from : https://codereview.stackexchange.com/questions/260450/thin-c-wrapper-for-sqlite3-c-library
#pragma once

// uncomment below to print blob data as hex in ostream overload of column_values
// #define PRINT_BLOB_AS_HEX

#include "sqlite3.h"

#include <string>
#include <vector>
#include <variant>
#include <cstdint>
#include <iostream>
#include <map>

namespace sql {

	/*
	sqlite types can be: NULL, INTEGER, REAL, TEXT, BLOB
	NULL: we don't support this type
	INTEGER: int
	REAL: double
	TEXT: std::string
	BLOB: std::vector<uint8_t>
	*/
	using sqlite_data_type = std::variant<int64_t, double, std::string, std::vector<uint8_t> >;

	using sqlite_result = std::vector<std::map<std::string, sql::sqlite_data_type>>;

	struct column_values {
		std::string column_name;
		sqlite_data_type column_value;
	};

	struct where_binding {
		std::string column_name;
		sqlite_data_type column_value;
	};

	extern std::ostream& operator<< (std::ostream& os, const column_values& v);
	extern std::ostream& operator<< (std::ostream& os, const sqlite_data_type& v);

	extern std::ostream& operator<< (std::ostream& os, const std::map<std::string, sqlite_data_type>& v);


	class sqlite {
	public:
		/* sqlite constructor */
		sqlite();

		/* cleanup */
		~sqlite();

		/* database must be opened before calling an sql operation */
		int open(const std::string& filename);

		/* close database connection */
		// int close();

		/* INSERT INTO (col1, col2) VALUES (:col1, :col2); table_name is table to insert into
		fields are a list of column name to column value key value pairs */
		int insert_into(const std::string& table_name, const std::vector<column_values>& fields) const;

		/* returns rowid of last successfully inserted row. If no rows
		inserted since this database connectioned opened, returns zero. */
		int last_insert_rowid();

		/* UPDATE contacts SET col1 = value1, col2 = value2, ... WHERE rowid = therowid;
		table_name is table to update, 
		fields are a list of column name to column value key value pairs to update
		where_clause is WHERE clause predicate expressed as : parameterised query
		bindings is the binding of values in the where clause */
		int update(
			const std::string& table_name, 
			const std::vector<column_values>& fields, 
			const std::string& where_clause, 
			const std::vector<where_binding>& bindings) const;

		/* UPDATE contacts SET col1 = value1, col2 = value2, ...;
		same as update(table_name, fields, where) except no WHERE clause so potential to change EVERY row. USE WITH CAUTION. */
		int update(const std::string& table_name, const std::vector<column_values>& fields) const;

		/* DELETE FROM table_name WHERE condition; */
		int delete_from(const std::string& table_name, const std::string& where_clause, const std::vector<where_binding>& bindings) const;

		/* DELETE FROM table_name;
		same as delete_from(table_name, where) except no WHERE clause so potential to delete EVERY row. USE WITH CAUTION. */
		int delete_from(const std::string& table_name) const;

		/* SELECT * FROM table_name WHERE col1 = x;
		table_name is table to select,
		where_clause is WHERE clause predicate expressed as : parameterised query
		bindings is the binding of values in the where clause
		results is a table of values */
		int select_star(const std::string& table_name,
			const std::string& where_clause,
			const std::vector<where_binding>& bindings,
			sqlite_result& results) const;
		/* SELECT * FROM table_name;
		table_name is table to select,
		results is a table of values */
		int select_star(const std::string& table_name,
			sqlite_result& results) const;

		/* SELECT col1, col2 FROM table_name WHERE col1 = x;
		table_name is table to select,
		fields are list of fields in table to select
		where is list of key value pairs as criterion for select
		results is a table of values */
		int select_columns(const std::string& table_name,
			const std::vector<std::string>& fields,
			const std::string& where_clause,
			const std::vector<where_binding>& bindings,
			sqlite_result& results) const;

		/* get error text relating to last sqlite error. Call this function
		whenever an operation returns a sqlite error code */
		const std::string get_last_error_description() const;

		inline sqlite3 *sqlitedb() const { return this->db_.get(); }

	private:

		std::shared_ptr<sqlite3> db_;
		// sqlite3* db_;
	};

} // itel
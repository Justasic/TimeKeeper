COMMONFLAGS=-Wall -Iinclude -Ivendor -Ivendor/sqlite -Ivendor/fmt/include -g
# 
# doing -D_BSD_SOURCE gets rid of a warning about strdup when using C11
CFLAGS=-pipe -std=c11 -D_DEFAULT_SOURCE=1 -D_FORTIFY_SOURCE=0 $(COMMONFLAGS)
CXXFLAGS=-pipe -std=c++20 $(COMMONFLAGS)
CC=$(HOME)/Projects/toolchains/llvm_musl_x86_64-2022-08-26/bin/clang
CXX=$(HOME)/Projects/toolchains/llvm_musl_x86_64-2022-08-26/bin/clang++
BUILDDIR=build
INSTALL=-m 755 -o root -g root
INSTALLPATH=/usr/local

###################################

fmtlibsources := vendor/fmt/src/format.cc vendor/fmt/src/os.cc
SRCS := $(shell find src/ -name '*.cpp' -or -name '*.c') $(fmtlibsources) vendor/sqlite/sqlite3.c vendor/sqlite/sqlite.cpp
OBJS := $(SRCS:%=$(BUILDDIR)/%.o)

$(BUILDDIR)/TimeKeeper: $(OBJS)
	$(CXX) -static -lc++abi $(OBJS) -o $(BUILDDIR)/TimeKeeper

# Pre-compile the necessary header files to make compilation faster
$(BUILDDIR)/vendor.pch:
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -x c++-header -c include/vendor.h -o $(BUILDDIR)/vendor.pch

$(BUILDDIR)/%.c.o: %.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@

$(BUILDDIR)/%.cpp.o: %.cpp $(BUILDDIR)/vendor.pch
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -include-pch $(BUILDDIR)/vendor.pch -c $< -o $@

$(BUILDDIR)/%.cc.o: %.cc
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)/

install: $(BUILDDIR)/TimeKeeper
	install $(INSTALL) $(BUILDDIR)/TimeKeeper $(INSTALLPATH)/bin/TimeKeeper

uninstall: remove
remove:
	rm -f $(INSTALLPATH)/bin/TimeKeeper
